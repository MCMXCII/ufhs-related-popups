<?php
class Ufhs_Relatedpopup_Block_Minicart extends Mage_Core_Block_Template
{
	private function _getCart()
	{
		return Mage::getSingleton('checkout/cart')->getQuote();
	}

	public function getCount()
	{
		return $this->_getCart()->getItemsCount();
	}

	public function getSubtotal()
	{
		return $this->_getCart()->getGrandTotal();
	}
}