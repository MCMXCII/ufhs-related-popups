<?php

class Ufhs_Relatedpopup_Model_PageCache_Popup extends Enterprise_PageCache_Model_Container_Abstract
{
	public function applyWithoutApp(&$content)
	{
		return false;
	}

	protected function _renderBlock()
    {
        return Mage::helper('relatedpopup')->showPopupHtml();
    }
}